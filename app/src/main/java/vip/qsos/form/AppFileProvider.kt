package vip.qsos.form

import androidx.core.content.FileProvider

/**
 * @author : 华清松
 * 文件共享目录
 */
open class AppFileProvider : FileProvider()
